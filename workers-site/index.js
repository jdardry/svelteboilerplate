import { getAssetFromKV } from '@cloudflare/kv-asset-handler';
import moment from 'moment';

const DEBUG = false;
const version = 1;

addEventListener('fetch', event => {
    try {
        event.respondWith(handleEvent(event));
    } catch (e) {
        event.respondWith(new Response('Internal Error', { status: 500 }));
    }
});

async function handleEvent(event) {
    if (event.request.pathname && event.request.pathname.indexOf('/api/')) {
        return await handleApiCalls();
    }

    if (event.request.method === 'GET') {
        return await getWebResponse(event);
    }
}

async function handleApiCalls(event) {
    event.respondWith(new Response('Not Implemented', { status: 501 }));
}

async function getWebResponse(event) {
    const cache = caches.default;
    const isFile = /\.\w+$/.test(event.request.url);

    let localResult = await cache.match(event.request);

    if(localResult && Number(localResult.headers.get('Application-Version')) !== version) {
        localResult = null;
    }

    if (localResult) {
        return localResult;
    }

    let page;
    if (isFile) {
        try {
            page = await getAssetFromKV(event);
        } catch {}
    }

    if (!page) {
        page = await getAssetFromKV(event, {
            mapRequestToAsset: req =>
                new Request(`${new URL(req.url).origin}/index.html`, req),
        });
    }

    const response = new Response(page.body, {
        status: 200,
        statusText: 'OK',
        headers: page.headers,
    });

    response.headers.set('X-XSS-Protection', '1; mode=block');
    response.headers.set('X-Content-Type-Options', 'nosniff');
    response.headers.set('X-Frame-Options', 'DENY');
    response.headers.set('Referrer-Policy', 'unsafe-url');
    response.headers.set('Feature-Policy', 'none');
    response.headers.set('Application-Version', version);
    response.headers.set(
        'Expires',
        `${moment().add(1, 'days').format('ddd, D MMM YYYY HH:mm:SS')} GMT`
    );

    await cache.put(event.request, response.clone());
    return response;
}
