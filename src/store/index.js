import { SvelteStore, SvelteSubject } from './definitions';
import { Schema } from './persist';

const db = await Schema.create({
    name: 'demo-db',
    tables: [
        {
            name: 'persisted',
            columns: {
                id: { primaryKey: true, autoIncrement: true },
                input: { dataType: 'string' },
            },
        },
    ],
});

export let store = new SvelteStore({
    menu: {
        open: false,
    },
    data: {
        persisted: db.persisted,
        local: [],
    },
});

export { SvelteSubject };
