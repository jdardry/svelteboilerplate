import { BehaviorSubject } from 'rxjs';

export class SvelteSubject extends BehaviorSubject {
    constructor() {
        super(...arguments);
    }

    set() {
        this.next(...arguments);
    }

    pipe() {
        const result = super.pipe(...arguments);
        result.set = result.next;
        return result;
    }
}
