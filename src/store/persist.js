import * as JsStore from 'jsstore/dist/jsstore.commonjs2';
import {
    chain,
    cloneDeep,
    defaults,
    findKey,
    head,
    isArray,
    isFunction,
    isObject,
    zipWith,
} from 'lodash';
import { SvelteSubject } from './reactivity';

export const Queryable = (function () {
    // This wrapper allows us to define a private constructor, while maintaining 'instanceof' behvaiour
    class PublicQueryable {
        constructor() {
            throw new SyntaxError(
                'Queryables can only be instantiated via Queryable.create().'
            );
        }

        static async create(description, queryOpts) {
            return new Queryable(description, queryOpts);
        }
    }

    class BaseQuery {
        #connection;
        #operation;
        #orphanedTimeout;
        #result = undefined;
        #final = false;
        #orphaned = false;
        queryObject = {};
        get result() {
            return this.#result;
        }
        get isFinal() {
            return this.#final;
        }
        get isOrphaned() {
            return this.#orphaned;
        }
        final() {
            if (this.isFinal || this.isOrphaned) {
                return;
            }

            this.#final = true;
            Object.freeze(this.queryObject);
            return this;
        }

        constructor(
            operation,
            { connection },
            { allowOrphanedAccessors } = { allowOrphanedAccessors: false }
        ) {
            this.#operation = operation;
            this.#connection = connection;

            if (!allowOrphanedAccessors) {
                // Declare the error here, so as to maintain an accurate stack trace.
                const contextualError = new Error(
                    'An orphaned query has been detected. This can result in unpredictable behaviour and orphaned subscribers.' +
                        " To disable this message, call the orphan() method or instantiate your schema with 'allowOrphanedAccessors' set to 'true'."
                );
                this.#orphanedTimeout = setTimeout(() => {
                    this.#orphanedTimeout = null;
                    throw contextualError;
                }, 0);
            }
        }

        async exec() {
            this.final();
            clearTimeout(this.#orphanedTimeout);
            this.#result = await this.#connection[this.#operation](
                this.queryObject
            );
            this.#invokeCallbacks(this.#result);
            return this.#result;
        }

        orphan() {
            this.#orphaned = true;
            clearTimeout(this.#orphanedTimeout);
            return this;
        }

        #subscribers = [];
        #invokeCallbacks(result) {
            for (let i in this.#subscribers) {
                this.#subscribers[i](result);
            }
        }
        subscribe(callback) {
            this.#subscribers.push(callback);
            return () => {
                let ind = this.#subscribers.indexOf(callback);
                if (ind < 0) {
                    return;
                }

                this.#subscribers.splice(ind, 1);
            };
        }

        where(comparisonObject) {
            if (!isObject(comparisonObject) || isArray(comparisonObject)) {
                throw new Error('Invalid comparison object.');
            }

            this.queryObject.where = defaults(
                this.queryObject.where,
                comparisonObject
            );
            return this;
        }
    }

    class SelectQuery extends BaseQuery {
        constructor({ name }) {
            super('select', ...arguments);
            this.queryObject.from = name;
        }

        take(count) {
            this.queryObject.limit = count;
            return this;
        }

        skip(count) {
            this.queryObject.skip = count;
            return this;
        }

        orderBy(columns, types) {
            if (isArray(columns)) {
                this.queryObject.order = zipWith(columns, types, (x, y) =>
                    defaults({ by: x, type: y }, { type: 'asc' })
                );
            } else {
                this.queryObject.order = {
                    by: columns,
                    types,
                };
            }
            return this;
        }

        groupBy(columns) {
            this.queryObject.groupBy = columns;
            return this;
        }

        distinct() {
            this.queryObject.distinct = true;
            return this;
        }

        join(joiner) {
            if (!isObject(joiner) || isArray(joiner)) {
                throw new Error('Invalid comparison object.');
            }

            this.queryObject.join = defaults(this.queryObject.join, joiner);
            return this;
        }

        flatten(flatten = true) {
            this.queryObject.flatten = flatten;
            return this;
        }
    }

    class InsertQuery extends BaseQuery {
        constructor({ name }) {
            super('insert', ...arguments);
            this.queryObject = {
                into: name,
                return: true,
            };
        }

        value(value) {
            if (!isArray(value)) {
                value = [value];
            }
            this.queryObject.values = value;
            return this;
        }

        where() {
            return this;
        }
    }

    class UpsertQuery extends InsertQuery {
        constructor() {
            super(...arguments);
            this.queryObject.upsert = true;
        }
    }

    class UpdateQuery extends BaseQuery {
        constructor({ name }) {
            super('update', ...arguments);
            this.queryObject.in = name;
        }

        set(column, value) {
            if (!isArray(column)) {
                column = [column];
            }
            if (!isArray(value)) {
                value = [value];
            }

            this.queryObject.set = this.queryObject.set ?? {};

            for(let i in column) {
                this.queryObject.set[column[i]] = value[i];
            }
            
            return this;
        }
    }

    class DeleteQuery extends BaseQuery {
        constructor({ name }) {
            super('remove', ...arguments);
            this.queryObject.from = name;
        }
    }

    class UnionQuery extends BaseQuery {
        constructor() {
            super('union', ...arguments);
            this.queryObject = [];
        }

        select(query) {
            if (!(query instanceof SelectQuery)) {
                throw new Error('Unions can only be ran on select queries');
            }

            query.orphan();
            query.final();

            this.queryObject.push(query.queryObject);
        }

        where() {
            return this;
        }
    }

    class IntersectQuery extends BaseQuery {
        constructor() {
            super('intersect', ...arguments);
            this.queryObject = { queries: [] };
        }

        select(query) {
            if (!(query instanceof SelectQuery)) {
                throw new Error('Intersects can only be ran on select queries');
            }

            query.orphan();
            query.final();

            this.queryObject.queries.push(query.queryObject);
        }

        where() {
            return this;
        }
    }

    class QuerySubject extends SvelteSubject {
        #column;
        #query;
        #upsertQuery;
        #primary;

        constructor(
            { column, query, upsertQuery, primary, initial } = { initial: 0 }
        ) {
            super(initial);
            this.#column = column;
            this.#query = query;
            this.#upsertQuery = upsertQuery;
            this.#primary = primary;

            query.final();
            query.orphan();
            this.#upsertQuery.orphan();

            this.#pollDb();
        }

        async #updateDb(value) {
            let dbValue = head(await this.#query.exec());

            if (dbValue === undefined) {
                console.warn(
                    "Attempted to write to a record that doesn't exist. Please ensure your data stores have been correctly initialised.",
                    value
                );
                this.#pollDb();
                return;
            }

            if (this.#column) {
                dbValue[this.#column] = value;
                await this.#upsertQuery.value(dbValue).exec();
                this.#pollDb();
                return;
            }

            if (!this.#primary) {
                let primaryKey = chain(this.#primary).keys().head().value();
                if (!value.hasOwnProperty(primaryKey)) {
                    throw new Error(
                        'Attempted to update a record without specifying its primary key'
                    );
                }

                if (value[primaryKey] !== dbValue[primaryKey]) {
                    throw new Error('Key mismatch on update query');
                }

                await this.#upsertQuery.value(value).exec();
                this.#pollDb();
                return;
            }
        }

        async #pollDb() {
            // get from DB
            let value = await this.#query.exec(),
                holder;
            super.next(
                this.#column
                    ? ((holder = head(value)),
                      holder ? holder[this.#column] : null)
                    : value
            );
        }

        next(value) {
            this.#updateDb(value);
        }

        refresh() {
            // Call next without updating the DB.
            this.#pollDb();
        }
    }

    // The actual class we want to implement.
    class Queryable {
        #name;
        #connection;
        #subscribers = [];
        #queryOpts = {};
        #columns = {};

        get primaryField() {
            const pfInd = findKey(this.#columns, x => x.primaryKey);
            if (!pfInd) {
                return undefined;
            }

            let result = {};
            return (result[pfInd] = this.#columns[pfInd]), result;
        }

        constructor({ name, connection, columns }, queryOpts) {
            // Allows 'instanceof' to function.
            this.__proto__.__proto__ = PublicQueryable.prototype;

            this.#name = name;
            this.#connection = connection;
            this.#columns = columns;
            this.#queryOpts = queryOpts;
        }

        get select() {
            return new SelectQuery(
                {
                    name: this.#name,
                    connection: this.#connection,
                },
                this.#queryOpts
            );
        }

        get union() {
            return new UnionQuery(
                {
                    name: this.#name,
                    connection: this.#connection,
                },
                this.#queryOpts
            );
        }

        get intersect() {
            return new IntersectQuery(
                {
                    name: this.#name,
                    connection: this.#connection,
                },
                this.#queryOpts
            );
        }

        get upsert() {
            const result = new UpsertQuery(
                {
                    name: this.#name,
                    connection: this.#connection,
                },
                this.#queryOpts
            );

            const unsub = result.subscribe(value => {
                if (!result.isOrphaned) {
                    unsub();
                }
                this.#invokeCallbacks(value);
            });

            return result;
        }

        get insert() {
            const result = new InsertQuery(
                {
                    name: this.#name,
                    connection: this.#connection,
                },
                this.#queryOpts
            );

            const unsub = result.subscribe(value => {
                if (!result.isOrphaned) {
                    unsub();
                }
                this.#invokeCallbacks(value);
            });

            return result;
        }

        get update() {
            const result = new UpdateQuery(
                {
                    name: this.#name,
                    connection: this.#connection,
                },
                this.#queryOpts
            );

            const unsub = result.subscribe(value => {
                if (!result.isOrphaned) {
                    unsub();
                }
                this.#invokeCallbacks(value);
            });

            return result;
        }

        get delete() {
            const result = new DeleteQuery(
                {
                    name: this.#name,
                    connection: this.#connection,
                },
                this.#queryOpts
            );

            const unsub = result.subscribe(value => {
                if (!result.isOrphaned) {
                    unsub();
                }
                this.#invokeCallbacks(value);
            });

            return result;
        }

        observable({ column, query, initial } = { upsert: true }) {
            if (!(query instanceof SelectQuery)) {
                throw new TypeError('selectQuery is not of correct type.');
            }

            if (column && query.queryObject.limit !== 1) {
                throw new Error(
                    'Column query bindings must have a select.take of 1.'
                );
            }

            const upsertQuery = new UpsertQuery(
                {
                    name: this.#name,
                    connection: this.#connection,
                },
                this.#queryOpts
            );

            upsertQuery.subscribe(() => this.#invokeCallbacks());

            const observable = new QuerySubject({
                column,
                query,
                upsertQuery,
                initial,
                primary: this.primaryField,
            });

            this.subscribe(() => observable.refresh());

            return observable;
        }

        #invokeCallbacks(value) {
            for (let i in this.#subscribers) {
                this.#subscribers[i](value);
            }
        }
        subscribe(callback) {
            this.#subscribers.push(callback);
            return () => {
                let ind = this.#subscribers.indexOf(callback);
                if (ind < 0) {
                    return;
                }

                this.#subscribers.splice(ind, 1);
            };
        }
    }

    return PublicQueryable;
})();

export const Schema = (function () {
    // This wrapper allows us to define a private constructor, while maintaining 'instanceof' behvaiour
    class PublicSchema {
        constructor() {
            throw new SyntaxError(
                'Schemas can only be instantiated via Schema.create().'
            );
        }

        static async create({ name, tables, version, migration }, queryOpts) {
            if (!name) {
                throw new Error('Name cannot be undefined');
            }

            version = version ?? 1;

            let connection = new JsStore.Connection(
                new Worker('/build/jsstore.worker.js')
            );

            for(let i in tables) {
                tables[i].version = version;
            }

            const currentVersion = await connection.getDbVersion(name);
            let stashed = {};
            if (currentVersion < version && isFunction(migration)) {
                const currentSchema = await connection.getDbSchema(name);
                if (currentSchema) {
                    await connection.initDb(currentSchema);

                    for (let i in currentSchema.tables) {
                        const tableName = currentSchema.tables[i].name;
                        stashed[tableName] = await connection.select({
                            from: tableName,
                        });
                    }
                }
                stashed = migration(cloneDeep(stashed));
            }

            const created = await connection.initDb({
                name,
                tables,
            });

            if (created && currentVersion < version) {
                for (let i in tables) {
                    const tableName = tables[i].name;
                    if (!(tableName in stashed)) {
                        continue;
                    }

                    connection.insert({
                        into: tableName,
                        values: stashed[tableName],
                    });
                }
            }

            let schema = new Schema();
            await this.#configureSelf.call(
                schema,
                connection,
                tables,
                queryOpts
            );
            return schema;
        }

        static async #configureSelf(connection, tables, queryOpts) {
            for (let i in tables) {
                Object.defineProperty(this, tables[i].name, {
                    value: await Queryable.create(
                        { connection, ...tables[i] },
                        queryOpts
                    ),
                    writable: false,
                });
            }
        }
    }

    // The actual class we want to implement.
    class Schema {
        constructor() {
            // Allows 'instanceof' to function.
            this.__proto__.__proto__ = PublicSchema.prototype;
        }
    }

    return PublicSchema;
})();

export const DataType = {
    String: 'string',
    Object: 'object',
    Array: 'array',
    Number: 'number',
    Boolean: 'boolean',
    Null: 'null',
    DateTime: 'date_time',
};
