import { BehaviorSubject, combineLatest, isObservable } from 'rxjs';
import {
    isFunction,
    isObject,
    values,
    remove,
    isArray,
    bind,
    defaults,
} from 'lodash';
import { get as $get } from 'svelte/store';
import { SvelteSubject } from './reactivity';
import { Schema, Queryable } from './persist';

export class SvelteStore extends BehaviorSubject {
    #internalState = {};
    #subscriber = null;
    #dirtyTimer = null;

    get isDirty() {
        return this.#dirtyTimer !== null;
    }

    constructor(template) {
        super();
        if (!isObject(template)) {
            throw new TypeError('SvelteStores can only be set to objects');
        }

        if (template instanceof SvelteStore) {
            template = $get(template);
        }

        this.#determineProps(template);
        this.#hookBindings();
        this.#markDirty({ immediate: true });
    }

    next(value) {
        if (!isObject(value)) {
            throw new TypeError('SvelteStores can only be set to objects');
        }

        if (value instanceof SvelteStore) {
            value = $get(value);
        }

        this.#unhookBindings();
        this.#determineProps(value);
        this.#hookBindings();

        this.#markDirty();
    }

    #determineProps(template, state = null) {
        state = state ?? this.#internalState;

        const templateProps = Object.getOwnPropertyNames(template);
        let stateProps = Object.getOwnPropertyNames(state);

        for (let i in templateProps) {
            const key = templateProps[i];
            stateProps = remove(stateProps, key);

            if (
                !state.hasOwnProperty(key) ||
                (isObject(template[key]) &&
                    !isFunction(template[key]) &&
                    !isArray(template[key]))
            ) {
                this.#defineProp(key, template, state);
                continue;
            }

            if (state[key] instanceof Queryable) {
                throw new Error(
                    'Queryables cannot be overwritten in the store.'
                );
            }

            let value = template[key];
            if (
                isArray(template[key]) &&
                !(template[key] instanceof CallbackArray)
            ) {
                value = new CallbackArray(
                    bind(this.#markDirty, this),
                    ...template[key]
                );
            }

            state[key] = value;
        }

        for (let i in stateProps) {
            delete this[stateProps[i]];
            delete state[stateProps[i]];
        }
        this.#markDirty();
    }

    #unhookBindings() {
        this.#subscriber && this.#subscriber.unsubscribe();
    }

    #hookBindings() {
        this.#subscriber = combineLatest(
            values(this.#internalState).filter(isObservable)
        ).subscribe(() => {
            this.#markDirty();
        });
    }

    #defineProp(key, template, state = null) {
        state = state ?? this.#internalState;
        let propObserver;

        if (template[key] instanceof Queryable) {
            state[key] = template[key];
            state[key].subscribe(() => this.#markDirty());
            Object.defineProperty(this, key, {
                get() {
                    return state[key];
                },
                configurable: false,
                enumerable: true,
            });
            return;
        }

        if (
            template[key] instanceof SvelteStore ||
            (isObject(template[key]) &&
                !isFunction(template[key]) &&
                !isArray(template[key]))
        ) {
            propObserver = new SvelteStore(template[key]);
        } else if (
            isArray(template[key]) &&
            !(template[key] instanceof CallbackArray)
        ) {
            propObserver = new CallbackArray(
                bind(this.#markDirty, this),
                ...template[key]
            );
        } else {
            propObserver = template[key];
        }

        state[key] = propObserver;
        Object.defineProperty(this, key, {
            get() {
                return state[key];
            },
            set(value) {
                if (
                    !(propObserver instanceof SvelteStore) &&
                    isObject(value) &&
                    !isFunction(value) &&
                    !isArray(value)
                ) {
                    this.#unhookBindings();
                    delete state[key];
                    propObserver = new SvelteStore(value);
                    state[key] = propObserver;
                    this.#hookBindings();
                    return;
                }
                if (
                    isArray(value) &&
                    !(value.constructor instanceof CallbackArray)
                ) {
                    value = new CallbackArray(
                        bind(this.#markDirty, this),
                        ...value
                    );
                }

                propObserver = value;
                state[key] = propObserver;

                this.#markDirty();
            },
            configurable: true,
            enumerable: true,
        });
    }

    #markDirty({ immediate } = { immediate: false }) {
        const handler = () => {
            let state = this.#internalState;
            if (this.additionalState) {
                state = defaults(state, this.additionalState);
            }
            super.next(state);
            this.#dirtyTimer = null;
        };

        if (this.#dirtyTimer !== null) {
            clearTimeout(this.#dirtyTimer);
        }

        if (immediate) {
            handler();
            return;
        }

        this.#dirtyTimer = setTimeout(handler, 0);
    }

    markDirty(context) {
        if (context !== this) {
            throw new SyntaxError(
                'Stores can only be marked dirty by themselves.'
            );
        }

        this.#markDirty();
    }
}

class CallbackArray extends Array {
    constructor(callback, ...args) {
        super(...args);
        this.constructor = super.constructor;
        return new Proxy(this, {
            set() {
                const success = Reflect.set(...arguments);
                callback();
                return success;
            },
        });
    }
}

export { Schema, SvelteSubject };

export function get(value) {
    return new Promise(resolve => {
        if(!isFunction(value?.subscribe)) {
            resolve(value);
            return;
        }

        value.subscribe(x => resolve(x));
    });
}