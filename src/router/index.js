import { SvelteRouter, Middleware } from './definitions';
import Router from './Router.svelte';
import Home from 'pages/index.svelte';
import Test from 'pages/Test.svelte';
import Persisted from 'pages/Persisted.svelte';
import Parameters from 'pages/Parameters.svelte';
import ErrorMessage from 'pages/ErrorMessage.svelte';
import Guarded from 'pages/Guarded.svelte';
import { head } from 'lodash';

export const routes = {
    home: {
        path: '/',
        title: 'My Title',
        component: Home,
    },
    test: {
        path: 'test/:param?',
        title: 'Test page',
        component: Test,
    },
    persisted: {
        path: 'persist',
        title: 'Persisted Data',
        component: Persisted,
    },
    specificity: {
        path: 'test/specific/:param?',
        title: 'specificity test',
        component: Test,
    },
    'multiple-parameters': {
        path: 'test/:first/:second/:third',
        title: 'multiple parameters',
        component: Parameters,
    },
    guarded: {
        path: 'guarded-url',
        title: "You shouldn't see this.",
        guards: [
            () => void 0,
            x => {
                return routes.guard;
            },
        ],
        component: Test,
    },
    guard: {
        title: 'Guarded',
        component: Guarded,
        meta: {
            navigable: false,
        },
    },
    '*': {
        title: '404',
        component: ErrorMessage,
        meta: {
            navigable: false,
        },
    },
};

class CustomMiddleware extends Middleware {
    before({ options, next }) {
        options.path = head(decodeURIComponent(options.path).split('|'));
        next({ options });
    }
}

export const router = new SvelteRouter(routes, [new CustomMiddleware()]);
export { Router };
