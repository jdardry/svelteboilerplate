import { SvelteStore } from 'store/definitions';
import {
    _,
    defaults,
    conforms,
    every,
    isArray,
    isFunction,
    isNull,
    isObject,
    isString,
    isUndefined,
    partial,
    values,
    head,
    set,
    flow,
    partialRight,
    bind,
    filter,
    get,
    trim,
    tap,
    last,
    reverse,
    spread,
    split,
    eq,
    trimStart,
    zipObject,
    trimEnd,
    isBoolean,
    thru,
    cloneDeep,
    map,
    unary,
} from 'lodash';
import { get as $get } from 'svelte/store';
function any(...predicates) {
    return source => {
        let result = source;
        for (var i in predicates) {
            result = predicates[i](source);
            if (result) {
                return result;
            }
        }
        return result;
    };
}

function all(...predicates) {
    predicates = predicates ?? [];
    return source => {
        let result = source;
        for (var i in predicates) {
            result = predicates[i](source);
            if (!result) {
                return result;
            }
        }
        return result;
    };
}

const stubFunc = () => void 0;

function flipBool(source) {
    if (!isBoolean(source)) {
        return source;
    }
    return !source;
}

function invoke(source) {
    if (!isFunction(source)) {
        return source;
    }

    return source();
}

const routeConform = conforms({
    path: any(isString, isNull),
    title: isString,
    component: isObject,
    meta: any(isObject, isNull, isUndefined),
    guards: any(
        all(isArray, partialRight(every, isFunction)),
        isNull,
        isUndefined
    ),
});

export class SvelteRouter extends SvelteStore {
    #middleware = [];

    constructor(routes, middleware = []) {
        if (!middleware.every(x => x instanceof Middleware)) {
            throw new Error('Invalid middleware passed to constructor');
        }

        for (var key in routes) {
            routes[key] = defaults(routes[key], {
                path: null,
                title: null,
                component: null,
                meta: null,
                guards: [],
            });

            if (!routeConform(routes[key])) {
                throw new TypeError(
                    `Router: '${key}' does not conform to expected route definition.`
                );
            }
        }

        super({
            ...routes,
        });

        this.#middleware = middleware;

        const internalcurrent = new SvelteStore({});
        Object.defineProperty(this, 'current', {
            get() {
                return internalcurrent;
            },
            set: value => {
                internalcurrent.next(value);
                super.markDirty(this);
            },
            enumerable: false,
        });

        this.go({ path: window.location.pathname, replace: true });
        window.addEventListener('popstate', () =>
            this.go({ path: window.location.pathname, supress: true })
        );
    }

    match(route) {
        const urlParts = trim(route, '/').split('/');
        return flow(
            $get,
            values,
            partialRight(
                filter,
                flow(
                    all(
                        flow(
                            partialRight(get, 'path'),
                            any(isNull, isUndefined),
                            flipBool
                        ),
                        flow(
                            partialRight(get, 'path'),
                            all(
                                x => x !== null && x !== undefined,
                                flow(
                                    partialRight(trim, '/'),
                                    partialRight(split, '/'),
                                    all(
                                        any(
                                            flow(
                                                partialRight(get, 'length'),
                                                partial(eq, urlParts.length)
                                            ),
                                            flow(
                                                x =>
                                                    x.length -
                                                    (last(last(x)) === '?'
                                                        ? 1
                                                        : 0),
                                                partial(eq, urlParts.length)
                                            )
                                        ),
                                        x => {
                                            for (let i in x) {
                                                if (
                                                    head(x[i]) === ':' &&
                                                    (last(x[i]) === '?' ||
                                                        i < x.length)
                                                ) {
                                                    continue; // skip optional parameters
                                                }

                                                if (urlParts[i] !== x[i]) {
                                                    return false;
                                                }
                                            }

                                            return true;
                                        }
                                    )
                                )
                            )
                        )
                    )
                )
            ),
            reverse,
            head,
            x => (x && (x.subscribe ? $get(x) : x)),
            //set parameters here, after all the routing is done.
            current => {
                if(!current) {
                    return;
                }
                current = cloneDeep(current);
                current.url = route;
                const [rootPath, ...parameters] = trim(current.path, '/')
                    .split(':')
                    .map(x => trim(x, '/'));
                const isFinalOptional = last(last(parameters)) === '?';
                const inputParameters = trimStart(trim(route, '/'), rootPath)
                    .split('/')
                    .filter(x => x !== '');
                if (
                    current.path !== null &&
                    inputParameters.length !== parameters.length &&
                    inputParameters.length !==
                        parameters.length - (isFinalOptional ? 1 : 0)
                ) {
                    current = this['*'] ?? this.current;
                    current.parameters = {};
                } else {
                    current.parameters = zipObject(
                        parameters.map(x => trimEnd(x, '?')),
                        inputParameters.map(x => {
                            let num = Number(x);
                            return isNaN(num) ? x : num;
                        })
                    );
                }
                return current;
            },
            any(
                flow(
                    x => map(x?.guards, unary(partialRight(partial, x))),
                    spread(any),
                    invoke
                ),
                flow(
                    any(
                        partialRight(tap, stubFunc),
                        () => this['*'] ?? this.current
                    )
                )
            )
        )(this);
    }

    go(opts) {
        opts = isObject(opts)
            ? ((opts.url = opts.url ?? opts.path), opts)
            : { path: opts, url: opts };

        for (let i in this.#middleware) {
            if (!isFunction(this.#middleware[i].before)) {
                continue;
            }

            let next = false;
            let middlewareResult = this.#middleware[i].before({
                options: opts,
                next: ({ options } = {}) => (
                    (opts = options ?? opts), (next = true)
                ),
                router: this,
            });

            if (!next) {
                return middlewareResult;
            }
        }

        let determinedRoute = flow(
            bind(this.match, this),
            all(
                () =>
                    opts.path !== this.current.url ||
                    opts.replace === true ||
                    opts.supress === true,
                x => x
            )
        )(opts.path);

        for (let i in this.#middleware) {
            if (!isFunction(this.#middleware[i].after)) {
                continue;
            }

            let next = false;
            let middlewareResult = this.#middleware[i].after({
                options: opts,
                route: determinedRoute,
                next: ({ route, options } = {}) => {
                    opts = options ?? opts;
                    determinedRoute = route ?? determinedRoute;
                    next = true;
                },
                router: this,
            });

            if (!next) {
                return middlewareResult;
            }
        }

        if (determinedRoute) {
            this.current = determinedRoute;
        }

        if (opts.url !== location.pathname) {
            (() => {
                if (opts?.supress) {
                    return;
                }

                if (opts?.replace) {
                    history.replaceState(
                        {},
                        this.current.title,
                        `/${trim(opts.url, '/')}`
                    );
                    return;
                }

                history.pushState(
                    {},
                    this.current.title,
                    `/${trim(opts.url, '/')}`
                );
            })();
        }

        return determinedRoute;
    }

    link(node, options) {
        options = isObject(options) ? options : { path: options };
        const handler = bind(function () {
            this.go(options);
        }, this);
        node.addEventListener('click', handler);
        return {
            destroy() {
                node.removeEventListener('click', handler);
            },
        };
    }
}

export class Middleware {
    constructor() {
        if (!isFunction(this.before) && !isFunction(this.after)) {
            throw new Error(
                'Middleware requires either a before or after method to be implemented'
            );
        }
    }
}
