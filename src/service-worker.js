// Use a cacheName for cache versioning
var cacheName = 'svelte:1';

self.addEventListener('activate', e => {
    e.waitUntil(clients.claim());
});

self.addEventListener('install', function (e) {
    e.waitUntil(
        (async () => {
            const cache = await caches.open(cacheName);
            await cache.addAll([
                '/',
                '/webmanifest.json',
                '/build/bundle.css',
                '/build/bundle.js',
                '/build/jsstore.worker.js',
            ]);

            self.skipWaiting();
        })()
    );
});

self.addEventListener('fetch', function (event) {
    event.respondWith(
        (async () => {
            const cache = await caches.open(cacheName);
            const local =
                event.request.method === 'GET' &&
                event.request.url.indexOf(location.origin) === 0;
            const isFile = /\.\w+$/.test(event.request.url);

            let localResult =
                (await cache.match(event.request)) ||
                (local && !isFile && (await cache.match('/')));
            if (navigator.onLine && !localResult) {
                try {
                    if (local && isFile) {
                        await cache.add(event.request);
                        localResult = await cache.match(event.request);
                    }
                } catch {}
            }

            return localResult || (await fetch(event.request));
        })()
    );
});
