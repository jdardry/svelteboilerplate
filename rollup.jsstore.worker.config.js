import commonjs from '@rollup/plugin-commonjs';
import { terser } from 'rollup-plugin-terser';
import resolve from '@rollup/plugin-node-resolve';

const production = !process.env.ROLLUP_WATCH;

export default {
	input: 'src/jsstore.worker.js',
	output: {
		sourcemap: false,
        format: 'esm',
		name: 'app',
		file: 'public/build/jsstore.worker.js'
	},
	plugins: [
		resolve({
			browser: true,
			dedupe: ['svelte']
		}),
		commonjs(),
		terser()
	],
    treeshake: false,
	watch: {
		clearScreen: false
	}
};
